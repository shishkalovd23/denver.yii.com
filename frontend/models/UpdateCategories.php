<?php
namespace frontend\models;

use frontend\models\Categories;
use yii\base\Model;
use Yii;
use yii\db\Query;

/**
 * Signup form
 */
class UpdateCategories extends Model
{
  public $category_name;
  public $id;


    /**
     * @inheritdoc
     */
    public function rules()
    {

          return [
              ['category_name', 'filter', 'filter' => 'trim'],
              [['category_name'], 'required'],
              ['category_name', 'unique', 'targetClass' => '\frontend\models\Categories', 'message' => 'такая категория уже существует'],



        ];
    }


    // - создание пользывателя админом ASM
    public function update_categories()
    {
      $category = Yii::$app->request->post('category');

        if ($this->validate()) {

            $categories = Categories::findOne($_GET['id']);
            $categories->category_name = $this->category_name;
            $categories->updated_at =time();

            $param = Yii::$app->request->post('UpdateCategories');
            // var_dump($param);
            // die();

            if ($categories->save()) {

                $param = Yii::$app->request->post();
                $connection = Yii::$app->db;
                $transaction = $connection->beginTransaction();
                try {


                    $transaction->commit();
                }catch (\Exception $e) {
                    $transaction->rollBack();
//                    throw $e;
                    echo '';
                }

                // - создание/обновление права доступа если такие имеются
                

                $this->id = $categories->getId();
                // $news['categories'] = $category;
                return $categories;
            }else{
                echo json_encode($user->getErrors());
            }
        }
        return null;
    }

    public function findCategories($id){
        $model = Categories::findOne($id);
        $this->id = $model->id;
        $this->category_name = $model->category_name;

        return $this;
    }

}
