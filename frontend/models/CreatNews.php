<?php
namespace frontend\models;

use frontend\models\News;
use yii\base\Model;
use Yii;
use yii\db\Query;

/**
 * Signup form
 */
class CreatNews extends Model
{
    public $title;
    public $content;
    public $status;
    // public $created_at;
    // public $updated_at;
    public $categories;
    public $id;



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['content', 'filter', 'filter' => 'trim'],
            [['content'], 'required'],
            ['content', 'string'],


            ['status', 'integer'],


            ['title', 'filter', 'filter' => 'trim'],
            ['title', 'required'],

            ['title', 'string', 'max' => 255],
            ['title', 'unique', 'targetClass' => '\frontend\models\News', 'message' => 'такая статья уже существует'],




        ];
    }


    public function saveNews($news)
    {

      if ($this->validate()) {
          $connection = Yii::$app->db;
          $transaction = $connection->beginTransaction();
          try {

              $news = new News();

              $news->title = $this->title;
              $news->content = $this->content;
              $news->status = $this->status;
              $news->created_at =time();
              $param = Yii::$app->request->post('CreatNews');

              if ($news->save()) {


                Yii::$app->db->createCommand()->insert('news_assignment', [
                      'id_news' => $news->getId(),
                      'id_categories' => Yii::$app->request->post('categories'),
                  ])->execute();

              }
              $transaction->commit();
              return $news;
          }catch (\Exception $e) {
              $transaction->rollBack();

//                throw $e;
              echo "Ошибка создания новости,попробуйте снова <a href='create'>Попробывать снова</a>";
              die();

      }
    }
    return null;
  }

}
