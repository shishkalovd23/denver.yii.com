<?php
namespace frontend\models;

use frontend\models\Categories;
use yii\base\Model;
use Yii;
use yii\db\Query;

/**
 * Signup form
 */
class CreatCategories extends Model
{

    public $category_name;
    public $id;



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['category_name', 'filter', 'filter' => 'trim'],
            [['category_name'], 'required'],
            ['category_name', 'unique', 'targetClass' => '\frontend\models\Categories', 'message' => 'такая категория уже существует'],

        ];
    }


    public function saveCategories($categories)
    {
        // var_dump(Yii::$app->request->post('CreatCategories'));
        // die();
      if ($this->validate()) {

          $connection = Yii::$app->db;
          $transaction = $connection->beginTransaction();
          try {

              $categories = new Categories();

              $categories->category_name = $this->category_name;
              $categories->created_at = time();

              $param = Yii::$app->request->post('CreatCategories');


              if ($categories->save()) {


              }
              $transaction->commit();
              return $categories;
          }catch (\Exception $e) {
              $transaction->rollBack();

//                throw $e;
              echo "Ошибка создания новости,попробуйте снова <a href='create'>Попробывать снова</a>";
              die();

      }
    }
    return null;
  }

}
