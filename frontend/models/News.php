<?php
namespace frontend\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * User model
 *
 * @property integer $id
* @property string $categories
 * @property integer $status

 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property string $title
 * @property string $content

 */
class News extends ActiveRecord
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 1;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%news}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],

//            ['password','safe']
        ];
    }


    public static function findByNews($id_news)
    {
        return static::findOne(['id' => $id_news, 'status' => [self::STATUS_ACTIVE,self::STATUS_UPDATED]]);
    }


    public function getId()
    {
        return $this->getPrimaryKey();
    }



}
