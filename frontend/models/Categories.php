<?php
namespace frontend\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * User model
 *
 * @property integer $id
* @property string $categories
 * @property integer $status

 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property string $title
 * @property string $content

 */
class Categories extends ActiveRecord
{

  public function rules()
  {
      return [

          ['category_name', 'string'],
          ['category_name', 'unique', 'targetClass' => '\frontend\models\Categories', 'message' => 'такая категория уже существует'],
          ['updated_at', 'integer'],
          ['created_at', 'integer'],
//            ['password','safe']
      ];
  }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%categories}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }






    public static function findByCategories($id)
    {
        return static::findOne(['id_categories' => $id]);
    }


    public function getId()
    {
        return $this->getPrimaryKey();
    }



}
