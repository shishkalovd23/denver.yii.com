<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Categories;

/**
 * SearchUsers represents the model behind the search form about `common\models\User`.
 */
class SearchCategories extends Categories
{


    public $item_name;
    public $company_name;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'created_at', 'updated_at'], 'integer'],
            ['category_name', 'string'],
        ];
    }



    public function search($params)
    {
        $query = Categories::find();
        // var_dump($query);
        // die();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'category_name' => $this->category_name,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'category_name', $this->category_name])
            ->andFilterWhere(['like', 'id', $this->id]);
//            ->andFilterWhere(['like', 'auth_key', $this->auth_key])
//            ->andFilterWhere(['like', 'password_hash', $this->password_hash])
//            ->andFilterWhere(['like', 'password_reset_token', $this->password_reset_token])

//            ->andFilterWhere(['like', 'item_name', $this->item_name])
            // ->andFilterWhere(['like', 'companies.name', $this->company_name]);


        return $dataProvider;
    }
}
