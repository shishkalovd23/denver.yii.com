<?php
namespace frontend\models;

use frontend\models\News;
use yii\base\Model;
use Yii;
use yii\db\Query;

/**
 * Signup form
 */
class UpdateNews extends Model
{
  public $title;
  public $content;
  public $status;
  // public $created_at;
  // public $updated_at;
  public $categories;
  public $id;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
          ['content', 'filter', 'filter' => 'trim'],
          [['content'], 'required'],
          ['content', 'string'],


          ['status', 'integer'],


          ['title', 'filter', 'filter' => 'trim'],
          ['title', 'required'],

          ['title', 'string'],
          ['categories', 'string'],


        ];
    }


    // - создание пользывателя админом ASM
    public function update_news()
    {
      $category = Yii::$app->request->post('category');

        if ($this->validate()) {

            $news = News::findOne($_GET['id']);
            $news->title = $this->title;
            $news->status = $this->status;
            $news->content = $this->content;
            $news->updated_at =time();
            $news->categories =$category;
            $param = Yii::$app->request->post('UpdateNews');
            // var_dump($param);
            // die();

            if ($news->save()) {

                $param = Yii::$app->request->post();
                $connection = Yii::$app->db;
                $transaction = $connection->beginTransaction();
                try {


                    $transaction->commit();
                }catch (\Exception $e) {
                    $transaction->rollBack();
//                    throw $e;
                    echo '';
                }

                // - создание/обновление права доступа если такие имеются
                if ($param['categories']){
                    $categories = Yii::$app->db->createCommand('SELECT id_categories FROM news_assignment WHERE id_news = ' . $_GET['id'])->queryAll(\PDO::FETCH_ASSOC);

                    if(!empty($categories)){

                        $result = Yii::$app->db->createCommand()->update('news_assignment', [
                            'id_categories' => $param['categories']],'id_news = '. $_GET['id'])->execute();

                    }else{
                      Yii::$app->db->createCommand()->insert('news_assignment', [
                          'id_categories' => $param['categories'],
                          'id_news' =>  $_GET['id'],
                      ])->execute();
                    }

                }

                $this->id = $news->getId();
                // $news['categories'] = $category;
                return $news;
            }else{
                echo json_encode($user->getErrors());
            }
        }
        return null;
    }

    public function findNews($id){
        $model = News::findOne($id);
        $this->id = $model->id;
        $this->title =  $model->title;
        $this->content = $model->content;
        $this->status = $model->status;
        return $this;
    }

}
