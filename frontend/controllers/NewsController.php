<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\helpers\Url;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\CreatNews;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use frontend\models\News;
use frontend\models\SearchNews;
use frontend\models\UpdateNews;
use yii\filters\auth\HttpBearerAuth;
use Firebase\JWT\JWT;
use common\models\User;



/**
 * @SWG\Swagger(
 *     basePath="/",
 *     produces={"yii\web\JsonParser"},
 *     consumes={"application/x-www-form-urlencoded"},
 *     @SWG\Info(version="1.0", title="Simple API"),
 * )
 */
class NewsController extends Controller
{



    /**
   * {@inheritdoc}
   */
  public function behaviors()
  {
      return [
          'access' => [
              'class' => AccessControl::className(),
              'only' => ['logout', 'signup'],
              'rules' => [
                  [
                      'actions' => ['signup'],
                      'allow' => true,
                      'roles' => ['?'],
                  ],
                  [
                      'actions' => ['logout'],
                      'allow' => true,
                      'roles' => ['@'],
                  ],
              ],
          ],
          'verbs' => [
              'class' => VerbFilter::className(),
              'actions' => [
                  'logout' => ['post'],
              ],
          ],
      ];
  }

  /**
   * {@inheritdoc}
   */
  public function actions()
  {
      return [
          'error' => [
              'class' => 'yii\web\ErrorAction',
          ],
          'captcha' => [
              'class' => 'yii\captcha\CaptchaAction',
              'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
          ],
          //The document preview addesss:http://api.yourhost.com/site/doc
         'doc' => [
             'class' => 'light\swagger\SwaggerAction',
             'restUrl' => \yii\helpers\Url::to(['/news/api'], true),
         ],
       //The resultUrl action.
       'api' => [
           'class' => 'light\swagger\SwaggerApiAction',
           //The scan directories, you should use real path there.
           'scanDir' => [
               Yii::getAlias('../../vendor/light/yii2-swagger/src'),
               Yii::getAlias('../controllers'),
               Yii::getAlias('../models'),
               Yii::getAlias('../../common/models'),
           ],
           //The security key
           'api_key' => '123',
       ],

      ];
  }

  /**
   * Displays homepage.
   *
   * @return mixed
   */
   /**
    * @SWG\GET(path="/news/get-all",
    *   definition="About",
    *   type="object",
    *
    *   allOf={
    *     @SWG\Schema(
    *       @SWG\Property(property="categories", type="integer", description="Name App"),
    *       @SWG\Property(property="description", type="string", description="Detail Information App"),
    *       @SWG\Property(property="version", type="string", description="Version APP"),
    *       @SWG\Property(property="baseUrl", type="string", description="Base Url APP")
    *     )
    *   }
    * )
    */
  public function actionGetAll()
  {

    if(Yii::$app->request->cookies->getValue('token')){
        $jwt = JWT::decode(Yii::$app->request->cookies->getValue('token'), Yii::$app->params['secretKey'],array(Yii::$app->params['algo']));
    }
    // var_dump($jwt);
    // die();
    $get = Yii::$app->request->get();


    if(isset($get['categories'])){
      $categories = Yii::$app->db->createCommand('SELECT * FROM categories' )->queryAll(\PDO::FETCH_ASSOC);
      $news = Yii::$app->db->createCommand('SELECT * FROM news  JOIN
        news_assignment ON `news`.`id` = `news_assignment`.`id_news`
        AND `news`.`status` = 1 AND `news_assignment`.`id_categories`= '.$get['categories'].'
        ORDER BY `news`.`created_at`' )->queryAll(\PDO::FETCH_ASSOC);
        // var_dump($news);
        // die();
    }else{
      $news = Yii::$app->db->createCommand('SELECT * FROM news WHERE status = 1 ORDER BY created_at' )->queryAll(\PDO::FETCH_ASSOC);
      $categories = Yii::$app->db->createCommand('SELECT * FROM categories' )->queryAll(\PDO::FETCH_ASSOC);
    }
    // if(!strpos($_SERVER['HTTP_REFERER'], 'doc')){
      return $this->render('news', ['categories' => $categories, 'news' => $news ]);
    // }else{
    //   return json_encode($categories);
    // }
  }

  public function actionGetOne()
  {
    $get = Yii::$app->request->get();
    if($get['id']){
      $categories = Yii::$app->db->createCommand('SELECT * FROM categories' )->queryAll(\PDO::FETCH_ASSOC);
      $news = Yii::$app->db->createCommand('SELECT * FROM news WHERE id='.$get['id'])->queryAll(\PDO::FETCH_ASSOC);
    }
      return $this->render('newsone', ['categories' => $categories, 'news' => $news ]);
  }
  public function actionIndex()
  {
    $searchModel = new SearchNews();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


    return $this->render('index', [
        'searchModel' => $searchModel,
        'dataProvider' => $dataProvider,
    ]);

  }

  public function actionCreate()
  {
    if(Yii::$app->request->cookies->getValue('token')){
        $jwt = JWT::decode(Yii::$app->request->cookies->getValue('token'), Yii::$app->params['secretKey'],array(Yii::$app->params['algo']));
    }
    if(isset($jwt->role) && ($jwt->role == 'user_admin' || $jwt->role == 'editor')){

      $model = new CreatNews();
      // var_dump($model->load(Yii::$app->request->post()));
      if ($model->load(Yii::$app->request->post())) {
        $model->saveNews($model);
        // var_dump($model);
         return $this->redirect(['index', 'id' => $model->id]);
      } else {
          return $this->render('create', [
              'model' => $model,
          ]);
      }
    }else{
       return Yii::$app->response->redirect(['news/get-all']);
    }
  }
  public function actionUpdate($id)
  {
    if(Yii::$app->request->cookies->getValue('token')){
        $jwt = JWT::decode(Yii::$app->request->cookies->getValue('token'), Yii::$app->params['secretKey'],array(Yii::$app->params['algo']));
    }
    if(isset($jwt->role) && ($jwt->role == 'user_admin' || $jwt->role == 'editor')){
      $model = new UpdateNews();

      if (!empty(Yii::$app->request->post()) && $model->load(Yii::$app->request->post()) && $model->update_news()) {


          return $this->redirect(['view', 'id' => $model->id]);
      } else {

          return $this->render('update', [
              'model' => $model->findNews($id),
          ]);
      }
    }else{
      return Yii::$app->response->redirect(['news/get-all']);
    }

  }
  public function actionDelete($id)
  {
    if(Yii::$app->request->cookies->getValue('token')){
        $jwt = JWT::decode(Yii::$app->request->cookies->getValue('token'), Yii::$app->params['secretKey'],array(Yii::$app->params['algo']));
    }
    if(isset($jwt->role) && $jwt->role == 'user_admin' ){
      $this->findModel($id)->delete();
      $result = Yii::$app->db->createCommand()->delete('news_assignment', 'id_news ='. $id)->execute();
      return $this->redirect(['index']);
    }else{
      return Yii::$app->response->redirect(['news/get-all']);
    }
  }
  protected function findModel($id)
  {
      if (($model = News::findOne($id)) !== null) {
          return $model;
      } else {
          throw new NotFoundHttpException('The requested page does not exist.');
      }
  }
  public function actionView($id)
  {

      return $this->render('view', [
          'model' => $this->findModel($id),
      ]);
  }
  /**
      * @SWG\POST(path="news/login",
      *     tags={"UserLogin"},
      *     summary="Retrieves the collection of User resources.",
      *     @SWG\Response(
      *         response = 200,
      *         description = "User collection response",
      *         @SWG\Schema(ref = "NewsController.php")
      *     ),
      * )
      */



  public function actionLogin()
  {
      if (!Yii::$app->user->isGuest) {
          return $this->goHome();
      }

      $model = new LoginForm();
      if ($model->load(Yii::$app->request->post()) && $model->login()) {
          return $this->goBack();
      } else {
          $model->password = '';

          return $this->render('login', [
              'model' => $model,
          ]);
      }
  }

  /**
   * Logs out the current user.
   *
   * @return mixed
   */
  public function actionLogout()
  {
      Yii::$app->response->cookies->remove('token');
      Yii::$app->user->logout();

      return $this->goHome();
  }

  /**
   * Displays contact page.
   *
   * @return mixed
   */
   public function actionSignup()
   {
       $model = new SignupForm();
       if ($model->load(Yii::$app->request->post())) {
           if ($user = $model->signup()) {
               if (Yii::$app->getUser()->login($user)) {
                   return $this->goHome();
               }
           }
       }

       return $this->render('signup', [
           'model' => $model,
       ]);
   }

   /**
    * Requests password reset.
    *
    * @return mixed
    */
   public function actionRequestPasswordReset()
   {
       $model = new PasswordResetRequestForm();
       if ($model->load(Yii::$app->request->post()) && $model->validate()) {
           if ($model->sendEmail()) {
               Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

               return $this->goHome();
           } else {
               Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
           }
       }

       return $this->render('requestPasswordResetToken', [
           'model' => $model,
       ]);
   }

   /**
    * Resets password.
    *
    * @param string $token
    * @return mixed
    * @throws BadRequestHttpException
    */
   public function actionResetPassword($token)
   {
       try {
           $model = new ResetPasswordForm($token);
       } catch (InvalidParamException $e) {
           throw new BadRequestHttpException($e->getMessage());
       }

       if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
           Yii::$app->session->setFlash('success', 'New password saved.');

           return $this->goHome();
       }

       return $this->render('resetPassword', [
           'model' => $model,
       ]);
   }



}


class BearerAuthController extends \yii\rest\ActiveController
{
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'bearerAuth' => [
                'class' => HttpBearerAuth::className()
            ]
        ]);
    }
}
