<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use frontend\models\Roles;


class RolesController extends Controller
{
    public function actionIndex(){
      $access = Roles::getRoles();
      echo json_encode($access);
    }

    public function actionCreate(){
      $access = Roles::getEmptyRole();
      echo json_encode($access);
    }
}
