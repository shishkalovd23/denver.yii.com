<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\helpers\Url;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use frontend\models\CreatCategories;
// use frontend\models\SignupForm;
// use frontend\models\ContactForm;
use frontend\models\Categories;
use frontend\models\SearchCategories;
use frontend\models\UpdateCategories;
use yii\filters\auth\HttpBearerAuth;
use Firebase\JWT\JWT;



class CategoriesController extends Controller
{

  public function actionIndex()
  {
    $searchModel = new SearchCategories();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


    return $this->render('index', [
        'searchModel' => $searchModel,
        'dataProvider' => $dataProvider,
    ]);

  }

  public function actionCreate()
  {
    if(Yii::$app->request->cookies->getValue('token')){
        $jwt = JWT::decode(Yii::$app->request->cookies->getValue('token'), Yii::$app->params['secretKey'],array(Yii::$app->params['algo']));
    }
    if(isset($jwt->role) && ($jwt->role == 'user_admin' || $jwt->role == 'editor')){

      $model = new CreatCategories();
      // var_dump($model->load(Yii::$app->request->post()));
      if ($model->load(Yii::$app->request->post())) {
        $model->saveCategories($model);
        // var_dump($model);
         return $this->redirect(['index', 'id' => $model->id]);
      } else {
          return $this->render('create', [
              'model' => $model,
          ]);
      }
    }else{
       return Yii::$app->response->redirect(['news/get-all']);
    }
  }

  public function actionDelete($id)
  {
    if(Yii::$app->request->cookies->getValue('token')){
        $jwt = JWT::decode(Yii::$app->request->cookies->getValue('token'), Yii::$app->params['secretKey'],array(Yii::$app->params['algo']));
    }
    if(isset($jwt->role) && $jwt->role == 'user_admin'){
      $this->findModel($id)->delete();
      $result = Yii::$app->db->createCommand()->delete('news_assignment', 'id_categories ='. $id)->execute();
      return $this->redirect(['index']);
    }else{
      return Yii::$app->response->redirect(['news/get-all']);
    }
  }
  protected function findModel($id)
  {
      if (($model = Categories::findOne($id)) !== null) {
          return $model;
      } else {
          throw new NotFoundHttpException('The requested page does not exist.');
      }
  }

  public function actionUpdate($id)
  {
    if(Yii::$app->request->cookies->getValue('token')){
        $jwt = JWT::decode(Yii::$app->request->cookies->getValue('token'), Yii::$app->params['secretKey'],array(Yii::$app->params['algo']));
    }
    if(isset($jwt->role) && ($jwt->role == 'user_admin' || $jwt->role == 'editor')){
      $model = new UpdateCategories();

      if (!empty(Yii::$app->request->post()) && $model->load(Yii::$app->request->post()) && $model->update_categories()) {


          return $this->redirect(['view', 'id' => $model->id]);
      } else {

          return $this->render('update', [
              'model' => $model->findCategories($id),
          ]);
      }
    }else{
      return Yii::$app->response->redirect(['news/get-all']);
    }

  }
  public function actionView($id)
  {

      return $this->render('view', [
          'model' => $this->findModel($id),
      ]);
  }


}
