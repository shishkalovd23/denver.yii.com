<?php

namespace frontend\controllers;

use common\models\UpdateUser;
use Yii;
use common\models\User;
use common\models\CreatUser;
use common\models\CreateRole;
use frontend\models\SearchUsers;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Query;
use Firebase\JWT\JWT;

/**
 * UserController implements the CRUD actions for User model.
 */

 /**
     * @SWG\Get(path="/user",
     *     tags={"User"},
     *     summary="Retrieves the collection of User resources.",
     *     @SWG\Response(
     *         response = 200,
     *         description = "User collection response",
     *         @SWG\Schema(ref = "common/models/User")
     *     ),
     * )
     */
class UserController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
      if(Yii::$app->request->cookies->getValue('token')){
          $jwt = JWT::decode(Yii::$app->request->cookies->getValue('token'), Yii::$app->params['secretKey'],array(Yii::$app->params['algo']));
      }
      if(isset($jwt->role) && $jwt->role == 'user_admin'){
        $searchModel = new SearchUsers();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        // var_dump($dataProvider);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
      }else{
         return Yii::$app->response->redirect(['news/get-all']);
      }
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
      if(Yii::$app->request->cookies->getValue('token')){
          $jwt = JWT::decode(Yii::$app->request->cookies->getValue('token'), Yii::$app->params['secretKey'],array(Yii::$app->params['algo']));
      }
      if(isset($jwt->role) && $jwt->role == 'user_admin'){
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
      }else{
         return Yii::$app->response->redirect(['news/get-all']);
      }
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
      if(Yii::$app->request->cookies->getValue('token')){
          $jwt = JWT::decode(Yii::$app->request->cookies->getValue('token'), Yii::$app->params['secretKey'],array(Yii::$app->params['algo']));
      }
      if(isset($jwt->role) && $jwt->role == 'user_admin'){
        $model = new CreatUser();
        if ($model->load(Yii::$app->request->post()) && $model->signup()) {
            return $this->redirect(['index', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
      }else{
         return Yii::$app->response->redirect(['news/get-all']);
      }
    }
    public function actionRole()
    {
      $param = Yii::$app->request->post('CreateRole');
            $model = new CreateRole();
            return $this->render('role', [
                'model' => $model,
            ]);

    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
      if(Yii::$app->request->cookies->getValue('token')){
          $jwt = JWT::decode(Yii::$app->request->cookies->getValue('token'), Yii::$app->params['secretKey'],array(Yii::$app->params['algo']));
      }
      if(isset($jwt->role) && $jwt->role == 'user_admin'){
        $model = new UpdateUser();

        if (!empty(Yii::$app->request->post()) && $model->load(Yii::$app->request->post()) && $model->update_user()) {
//            echo 'success';
            return $this->redirect(['view', 'id' => $model->id]);
        } else {

            return $this->render('update', [
                'model' => $model->findUser($id),
            ]);
        }
      }else{
         return Yii::$app->response->redirect(['news/get-all']);
      }

    }


    // public function actionUpdateUser($id)
    // {
    //     $model = $this->findModel($id);
    //
    //     $model->setPassword('Lzpv6x%RVG%avp');
    //
    //     if ($model->save()){
    //         echo 'success';
    //     }
    //
    // }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
      if(Yii::$app->request->cookies->getValue('token')){
          $jwt = JWT::decode(Yii::$app->request->cookies->getValue('token'), Yii::$app->params['secretKey'],array(Yii::$app->params['algo']));
      }
      if(isset($jwt->role) && $jwt->role == 'user_admin'){
        $this->findModel($id)->delete();
        $result = Yii::$app->db->createCommand()->delete('auth_assignment', 'user_id ='. $id)->execute();
        return $this->redirect(['index']);
      }else{
         return Yii::$app->response->redirect(['news/get-all']);
      }
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    

}
