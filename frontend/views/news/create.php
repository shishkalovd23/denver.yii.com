<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\News */

$this->title = 'Создать новость';
?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?= Html::encode($this->title) ?>

    </h1>
</section>

<!-- Main content -->
<section class="content">

    <div class="row">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>

</section>
