<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\SearchNews */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Список новостей';

?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?= Html::encode($this->title) ?>

    </h1>
    <p class="text-right">
        <?= Html::a('Создать новость', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <!-- <p class="text-right">
        <? //Html::a('Create New Role', ['role'], ['class' => 'btn btn-success']) ?>
    </p> -->
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">

<?
// var_dump($searchModel);
// die();
?>

                <div class="box-body table-responsive">

                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'summary' => "<p>Showing {begin} - {end} of {totalCount} items</p>",
                        'tableOptions' => ['class' => 'table table-bordered table-hover'],
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            'id',
                            [
                            'attribute' => 'title',
                            'label' => 'Заголовок',
                            ],
                            [
                            'attribute' => 'content',

                            'label' => 'Описание',
                            'options' => ['style' => ' height: 20px; max-height: 20px;'],
                            'contentOptions'=>['style'=>'white-space: normal;overflow: hidden;'],

                            ],
                            [
                            'attribute' => 'status',
                            'label' => 'Опубликована',
                            ],
                            [
                            'attribute' => 'created_at',
                            'format' => ['date', 'php:d/m/Y H:m'],
                            'label' => 'Создан',
                            ],



                            ['class' => 'yii\grid\ActionColumn'],
                        ],
                    ]); ?>

                </div>

            </div>
        </div>
    </div>
</section><!-- /.content -->
