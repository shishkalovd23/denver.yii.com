<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\db\Query;
use mihaildev\ckeditor\CKEditor



?>

<div class="user-form col-md-6 col-md-offset-3">
    <div class="box box-primary">



        <?php $form = ActiveForm::begin(); ?>

<?php $id = $_GET['id'];?>

        <div class="box-body">

            <?= $form->field($model, 'status')->dropDownList(['1' => '1','0' => '0'])->label('Опубликовать'); ?>

            <?= $form->field($model, 'title')->label('Заголовок'); ?>


            <?= $form->field($model, 'content')->widget(CKEditor::className(),['editorOptions' => ['preset' => 'full', 'inline' => false, ],])->label('Контент'); ?>



            <?php  $categories = Yii::$app->db->createCommand('SELECT category_name,id FROM categories' )->queryAll(\PDO::FETCH_ASSOC);
            // $news = Yii::$app->db->createCommand('SELECT category_name FROM categories WHERE id IN (SELECT id_categories From news_assignment WHERE id_news = '.$id.')' )->queryAll(\PDO::FETCH_ASSOC);
            $news = Yii::$app->db->createCommand('SELECT category_name FROM categories
                                                  JOIN  news_assignment ON `categories`.`id` = `news_assignment`.`id_categories`
                                                  AND `news_assignment`.`id_news`= '.$id )->queryAll(\PDO::FETCH_ASSOC);

            ?>


            <label class="control-label"> Текущая категория </label>
           <select size = "1" name = "category" class="form-control">
             <?php foreach ($news as $key => $value) { ?>
               <option  ><?=$value['category_name'] ?></option>
               <?php } ?>
           </select>
            <label class="control-label"> Категория </label>
                   <select size = "4" name = "categories" class="form-control">
                     <?php foreach ($categories as $key => $value) { ?>
                       <option value = "<?=$value['id'] ?>" ><?=$value['category_name'] ?></option>
                       <?php } ?>
                   </select>

            <!-- <?= $form->field($model, 'categories')->dropDownList($categories)->label('Категория'); ?> -->


        </div>
        <div id="add-forms"></div>

        <div class="box-footer text-right">
            <?= Html::submitButton('Update', ['class' =>  'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
