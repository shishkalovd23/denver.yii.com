<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\db\Query;
use mihaildev\ckeditor\CKEditor



?>

<div class="user-form col-md-6 col-md-offset-3">
    <div class="box box-primary">



        <?php $form = ActiveForm::begin(); ?>



        <div class="box-body">

            <?= $form->field($model, 'status')->dropDownList(['1' => '1','0' => '0'])->label('Опубликовать'); ?>

            <?= $form->field($model, 'title')->label('Заголовок'); ?>


            <?= $form->field($model, 'content')->widget(CKEditor::className(),['editorOptions' => ['preset' => 'full', 'inline' => false, ],])->label('Контент'); ?>



            <?php  $categories = Yii::$app->db->createCommand('SELECT category_name,id FROM categories' )->queryAll(\PDO::FETCH_ASSOC);

            // var_dump($idcategories);?>
            <label class="control-label"> Категория </label>
                   <select size = "4" name = "categories" class="form-control">
                     <?php foreach ($categories as $key => $value) { ?>
                       <option value = "<?=$value['id'] ?>" ><?=$value['category_name'] ?></option>
                       <?php } ?>
                   </select>




        </div>
        <div id="add-forms"></div>

        <div class="box-footer text-right">
            <?= Html::submitButton('Create', ['class' =>  'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
