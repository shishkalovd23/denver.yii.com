<?php
use yii\helpers\Url;
 ?>
<!DOCTYPE html>
<!-- <html lang="en"> -->
<!-- <head> -->
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>
    /* Set height of the grid so .sidenav can be 100% (adjust if needed) */
    .row.content {height: 1500px}

    /* Set gray background color and 100% height */
    .sidenav {
      background-color: #f1f1f1;
      height: 100%;
    }

    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }

    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height: auto;}
    }
  </style>
</head>
<!-- <body> -->
<!-- <?var_dump($categories);
var_dump($news);?> -->
<div class="container-fluid">
  <div class="row content">
    <div class="col-sm-3 sidenav">
      <h4>Категории</h4>
      <ul class="nav nav-pills nav-stacked">
        <?php foreach ($categories as $key => $value) {?>
            <li><a href="<?=Url::to([ 'get-all', 'categories'=>$value['id']])?>"><?=$value['category_name']?></a></li>
        <?php }?>
      </ul><br>

    </div>

    <div class="col-sm-9">
      <?php foreach ($news as $key => $value) {?>
          <h2><?=$value['title']?></h2>
          <h5><span class="glyphicon glyphicon-time"></span> <?=date('d:m:Y H:m',$value['created_at']);?></h5>

          <p><?=$value['content']?><b></b></p>

          <hr>
      <?php }?>
      </div>
    </div>
  </div>
</div>


<!-- </body>
</html> -->
