<?php
use yii\helpers\Url;
 ?>
<!DOCTYPE html>
<!-- <html lang="en"> -->
<!-- <head> -->
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>
    /* Set height of the grid so .sidenav can be 100% (adjust if needed) */
    .row.content {height: 1500px}

    /* Set gray background color and 100% height */
    .sidenav {
      background-color: #f1f1f1;
      height: 100%;
    }

    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }

    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height: auto;}
    }
  </style>
</head>
<!-- <body> -->
<!-- <?
var_dump($news);?> -->
<div class="container-fluid">
  <div class="row content">
    <div class="col-sm-3 sidenav">
      <h4>Категории</h4>
      <ul class="nav nav-pills nav-stacked">
        <?php foreach ($categories as $key => $value) {?>
            <li><a href="<?=Url::to([ '', 'categories'=>$value['id']])?>"><?=$value['category_name']?></a></li>
        <?php }?>
      </ul><br>

    </div>

    <div class="col-sm-9">
      <?php if(!empty($news)){?>
        <?php foreach ($news as $key => $value) {
          ?>
            <a href="<?=Url::to([ 'get-one', 'id'=>$value['id']])?>"><h2><?=$value['title']?></h2></a>
            <h5><span class="glyphicon glyphicon-time"></span> <?=date('d:m:Y H:m',$value['created_at']);?></h5>
            <!-- <p><?=$value['content']?></p> -->
            <p><?=mb_substr($value['content'], 0, 200);?><b> ...</b></p>
            <a href="<?=Url::to([ 'get-one', 'id'=>$value['id']])?>"> <b>Подробнее...</b> </a>
            <hr>
        <?php }?>
      <?php }else{?>
          <h4>В данной категории нет статей</h4>

      <?php }?>
      </div>
    </div>
  </div>
</div>


<!-- </body>
</html> -->
