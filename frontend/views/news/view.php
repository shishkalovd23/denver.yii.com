<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\User */

// $this->title = $model->name;
?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?= Html::encode($this->title) ?>
        <small>Optional description</small>
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="user-form col-md-6 col-md-offset-3">
            <div class="box box-primary">

                <div class="box-header with-border">
                    <h3 class="box-title">Quick Example</h3>
                </div>

                <div class="box-body">

                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'id',
                            'title',
                            'content',
            //            'auth_key',
            //            'password_hash',
            //            'password_reset_token',
                            'status',

                        ],
                    ]) ?>

                </div>

                <div class="box-footer text-right">
                    <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                    <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ]) ?>
                </div>

            </div>
        </div>
    </div>
</section>
