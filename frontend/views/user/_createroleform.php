<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\db\Query;
use Yii;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
//get roles

$roleuser = $group_list = Yii::$app->db->createCommand('SELECT name FROM auth_item WHERE name  NOT IN ("analysis","reports","monitoring","users","user_admin","administration")' )->queryAll(\PDO::FETCH_ASSOC);
?>

<div class="user-form col-md-6 col-md-offset-3">
    <div class="box box-primary">

        <div class="box-header with-border">
            <h3 class="box-title">Quick Example</h3>
        </div>

        <?php $form = ActiveForm::begin(); ?>

        <div class="box-body">


          <label class="control-label"> Список существующих ролей </label>
         <select size = "10" name = "idavto" class="form-control">
           <?php foreach ($roleuser as $key => $value) { ?>
             <option  ><?=$value['name'] ?></option>
             <?php } ?>
         </select>
          <label class="control-label"> Выберите группу пользователей </label>
           <?= $form->field($model, 'levelaccess')->dropDownList(['users' => 'users']) ?>
           <label class="control-label"> Введите название новой роли </label>
           <?= $form->field($model, 'role') ?>

            <!--    --><?//= $form->field($model, 'status')->textInput() ?>

        </div>
        <div id="add-forms"></div>

        <div class="box-footer text-right">
            <?= Html::submitButton('Role', ['class' =>  'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
