<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\db\Query;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
//get roles



?>

<div class="user-form col-md-6 col-md-offset-3">
    <div class="box box-primary">



        <?php $form = ActiveForm::begin(); ?>

        <div class="box-body">

          <?= $form->field($model, 'name') ?>

          <?= $form->field($model, 'surname') ?>

          <?= $form->field($model, 'email') ?>

          <?= $form->field($model, 'password') ?>

          <?= $form->field($model, 'password_repeat') ?>

          <?= $form->field($model, 'role')->dropDownList(['user_admin' => 'user_admin','editor' => 'editor']) ?>


        </div>
        <div id="add-forms"></div>

        <div class="box-footer text-right">
            <?= Html::submitButton('Create', ['class' =>  'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
