<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\SearchUsers */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Список пользователей';
// var_dump($dataProvider);
// die();
?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?= Html::encode($this->title) ?>

    </h1>
    <p class="text-right">
        <?= Html::a('Создать пользователя', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <!-- <p class="text-right">
        <? //Html::a('Create New Role', ['role'], ['class' => 'btn btn-success']) ?>
    </p> -->
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">

              <div class="box-body table-responsive">
                  <?php
                  // var_dump($created_at); ?>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        // $created_at = date('d-m-Y H:i:s',$searchModel['created_at']);
                        'summary' => "<p>Showing {begin} - {end} of {totalCount} items</p>",
                        'tableOptions' => ['class' => 'table table-bordered table-hover'],
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            'id',
                            [
                            'attribute' => 'name',
                            'label' => 'Имя пользователя',
                            ],

                            [
                            'attribute' => 'surname',

                            'label' => 'Фамилия',
                            ],
                            'email:email',
                            [
                            'attribute' => 'created_at',
                            'format' => ['date', 'php:d/m/Y H:m'],
                            'label' => 'Создан',
                            ],



                            ['class' => 'yii\grid\ActionColumn'],
                        ],
                    ]); ?>

                </div>

            </div>
        </div>
    </div>
</section><!-- /.content -->
