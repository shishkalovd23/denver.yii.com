<?php

use yii\helpers\Html;
?>

<div class="box box-primary">
    <div class="box-header with-border">
        <div class="form-group">
                 <label class="control-label"> Роли </label>
                    <?= Html::dropDownList('CreatUser[role]', 'name', $items) ?>

                    <div class="form-check form-check-inline">
                        <label class="form-check-label">

                          <input class="form-check-input" name = "EnableAvto" type="checkbox" id="inlineCheckbox1" value="transport">
                                Показывать все группы транспорта
                        </label>

                      </div>

        </div>

        <?php
        If(!empty($companies)){
        ?>
            <div class="form-group">
                <label class="control-label"> Компании </label>
                <?= Html::dropDownList('CreatUser[company_id]', 'name', $companies) ?>
            </div>

        <?php
        }
        ?>
    </div>
</div>
