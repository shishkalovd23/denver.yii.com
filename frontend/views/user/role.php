<?php

use yii\helpers\Html;
use yii\db\Query;
use yii\widgets\ActiveForm;
use Yii;


/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = 'Create Role';
?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?= Html::encode($this->title) ?>
        <small>Optional description</small>
    </h1>
</section>

<!-- Main content -->
<section class="content">

    <div class="row">
        <?= $this->render('_createroleform', [
            'model' => $model,
        ]) ?>
    </div>

</section>
