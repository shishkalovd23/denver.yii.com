<?php

use yii\helpers\Html;
use yii\db\Query;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = 'Редактировать пользователя: ' . ' ' . $model->name;

$query = new Query;
$id = $_GET['id'];
// $company = $query->select('routes')->from('companies')->where(['id' => Yii::$app->user->identity->companies_id])->all();

$roleuser = $group_list = Yii::$app->db->createCommand('SELECT item_name FROM auth_assignment WHERE user_id ='. $id  )->queryAll(\PDO::FETCH_ASSOC);
// var_dump($roleuser);
// if($company[0]['routes'] == 1){
   $role_items = $query->select('name')->from('auth_item')->all();
// } else {
//   $role_items = Yii::$app->db->createCommand('SELECT name FROM auth_item WHERE name  NOT IN ("routes","routes_create","routes_delete","routes_edit","routes_show_all")' )->queryAll(\PDO::FETCH_ASSOC);
// }
foreach($role_items as $value){
$items[$value['name']] = $value['name'];
}
?>

<!-- Content Header (Page header) -->


<section class="content">
    <div class="row">
        <div class="user-form col-md-6 col-md-offset-3">
            <div class="box box-primary">

                <div class="box-header with-border">
                    <h3 class="box-title">Редактирование пользователя</h3>
                </div>

                <?php $form = ActiveForm::begin(); ?>

                <div class="box-body">

                    <?= $form->field($model, 'id')->hiddenInput()->label(false) ?>

                    <?= $form->field($model, 'name')->label('Имя пользователя'); ?>

                    <?= $form->field($model, 'surname')->label('Фамилия'); ?>

                    <?= $form->field($model, 'email') ?>

                    <?= $form->field($model, 'password')->label('Пароль'); ?>

                     <label class="control-label"> Текущая роль пользователя </label>
                    <select size = "1" name = "idavto" class="form-control">
                      <?php foreach ($roleuser as $key => $value) { ?>
                        <option  ><?=$value['item_name'] ?></option>
                        <?php } ?>
                    </select>



                    <div class="form-group">
                        <?= $form->field($model, 'role')->dropDownList(array_merge([""=>""],$items))->label('Выберите Роль'); ?>
                    </div>

                    <div class="form__params form__params_separator">
                        <?php
                          echo $form->field($model, 'action')->radioList([

                                                                        2 => ''
                                                                      ])->label('Изменить роль');
                        ?>
                    </div>

                </div>


                <div class="box-footer text-right">
                    <?= Html::submitButton('Update', ['class' =>  'btn btn-success']) ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>
</section>
