<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\SearchNews */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Список категорий';

?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?= Html::encode($this->title) ?>

    </h1>
    <p class="text-right">
        <?= Html::a('Создать категорию', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <!-- <p class="text-right">
        <? //Html::a('Create New Role', ['role'], ['class' => 'btn btn-success']) ?>
    </p> -->
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">



                <div class="box-body table-responsive">

                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'summary' => "<p>Showing {begin} - {end} of {totalCount} items</p>",
                        'tableOptions' => ['class' => 'table table-bordered table-hover'],
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            'id',

                            [
                            'attribute' => 'category_name',
                            'label' => 'Категория',
                            'options' => ['style' => ' height: 20px; max-height: 20px;'],
                            'contentOptions'=>['style'=>'white-space: normal;'],

                            ],




                            ['class' => 'yii\grid\ActionColumn'],
                        ],
                    ]); ?>

                </div>

            </div>
        </div>
    </div>
</section><!-- /.content -->
