<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use Firebase\JWT\JWT;
// use Yii;

AppAsset::register($this);
?>
<?php
if(Yii::$app->request->cookies->getValue('token')){
    $jwt = JWT::decode(Yii::$app->request->cookies->getValue('token'), Yii::$app->params['secretKey'],array(Yii::$app->params['algo']));
}
// var_dump($jwt);
// die();
$this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'Все новости',
        'brandUrl' => Yii::$app->homeUrl,


        'options' => [
    'id'=>'navid',
    'class' => 'navbar-nav navbar-inverse navbar-fixed-top',
    'style'=>'float: left; font-size: 16px;',
    'data'=>'menu',
  ],
    ]);
      if(isset($jwt->role) && $jwt->role == 'user_admin'){

          $menuItems = [
              // ['label' => 'Каталог', 'url' => ['/product/get-all']],

              ['label' => 'Список новостей', 'url' => ['/news/index']],
              ['label' => 'Список категорий', 'url' => ['/categories/index']],
              // ['label' => 'Обновить товар', 'url' => ['/product/update']],
              ['label' => 'Список пользователей', 'url' => ['/user/index']],



    ];


  }else if(isset($jwt->role) &&  $jwt->role == 'editor' ){

    $menuItems = [

          ['label' => 'Список новостей', 'url' => ['/news/index']],
          ['label' => 'Список категорий', 'url' => ['/categories/index']],
      ];
  }
    if (Yii::$app->user->isGuest) {
        define('Guest','Гость');
        $menuItems[] = ['label' => 'Login (' . Guest . ')', 'url' => ['/news/login']];
    } else {
      if(Yii::$app->user->identity->name){
        $menuItems[] = '<li>'
            . Html::beginForm(['/news/logout'], 'post')
            . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->name . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';
      }
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; <?= Html::encode(Yii::$app->name) ?> <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
