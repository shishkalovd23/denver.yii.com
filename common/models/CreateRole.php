<?php
namespace common\models;

use common\models\User;
use yii\base\Model;
use Yii;
use yii\db\Query;

/**
 * Signup form
 */
class CreateRole extends Model
{
    // public $name;
    // public $surname;
    // public $email;
    // public $password;
    public $role;
    // public $action;
    // public $system_geozones;
    // public $id;
    // public $passwordRepeat;
     public $levelaccess;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'filter', 'filter' => 'trim'],
            [['name','surname'], 'required'],
            ['name', 'string', 'min' => 2, 'max' => 255],
            ['email', 'string', 'max' => 255],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],

            [['system_geozones'], 'boolean'],
            ['role', 'required'],

//            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Данный почтовый адрес уже занят'],

//            ['password', 'required'],
            ['password', 'string', 'min' => 6],
            [['passwordRepeat'], 'compare', 'compareAttribute' => 'password','message' => 'Пароли не совпадают'],
        ];
    }
  }
