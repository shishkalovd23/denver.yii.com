<?php
namespace common\models;

use common\models\User;
use yii\base\Model;
use Yii;
use yii\db\Query;

/**
 * Signup form
 */
class CreatUser extends Model
{
    public $name;
    public $surname;
    public $email;
    public $password;
    public $role;
    public $group;
    public $id;
    public $passwordRepeat;
    public $password_repeat;
    public $system_geozones;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'filter', 'filter' => 'trim'],
            [['name','surname','password'], 'required'],
            ['name', 'string', 'min' => 2, 'max' => 255],
            ['name', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Данное имя уже используется'],
            ['email', 'string', 'max' => 255],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Данный почтовый адрес уже занят'],



            ['password', 'required'],
            ['password', 'string', 'min' => 6],
            [['passwordRepeat'], 'compare', 'compareAttribute' => 'password','message' => 'Пароли не совпадают'],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
      if ($this->validate()) {
          $connection = Yii::$app->db;
          $transaction = $connection->beginTransaction();
          try {

              $user = new User();
              $param = Yii::$app->request->post('CreatUser');
              $user->name = $this->name;
              $user->surname = $this->surname;
              $user->email = $this->email;

              $user->setPassword($this->password);
              $user->generateAuthKey();
              

              // var_dump($user->generateAuthKey());die();

              // var_dump($user->save());die();
              if ($user->save()) {


                Yii::$app->db->createCommand()->insert('auth_assignment', [
                      'user_id' => $user->getId(),
                      'item_name' => $param['role'],
                  ])->execute();

              }
              $transaction->commit();
              return $user;
          }catch (\Exception $e) {
              $transaction->rollBack();

//                throw $e;
              echo "Ошибка создания пользывателя,попробуйте снова <a href='create'>Попробывать снова</a>";
              die();

}
}
return null;
}


}
