<?php
namespace common\models;

use common\models\User;
use yii\base\Model;
use Yii;
use yii\db\Query;

/**
 * Signup form
 */
class UpdateUser extends Model
{
    public $name;
    public $surname;
    public $email;
    public $password;
    public $role;
    public $action;

    public $id;
    public $passwordRepeat;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'filter', 'filter' => 'trim'],
            [['name','surname'], 'required'],
            ['name', 'string', 'min' => 2, 'max' => 255],
            // ['name', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Данное имя уже используется'],
            ['email', 'string', 'max' => 255],


            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],



//            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Данный почтовый адрес уже занят'],

//            ['password', 'required'],
            ['password', 'string', 'min' => 6],

        ];
    }


    // - создание пользывателя админом ASM
    public function update_user()
    {
      $param = Yii::$app->request->post('UpdateUser');
      // var_dump($param['action']);
      // die();
        if ($this->validate()) {

            $user = User::findOne($param['id']);
            $user->name = $this->name;
            $user->surname = $this->surname;
            $user->email = $this->email;
            // $user->password = $this->password;


            if ($this->password != null) {
                $user->setPassword($this->password);
                // $user->status = 20;
            }

            if ($user->save()) {

                $param = Yii::$app->request->post('UpdateUser');
                $connection = Yii::$app->db;
//
                if ($param['role']){


                      if ($param['role']  && $param['action'] == 2 ) {
                        Yii::$app->db->createCommand()->delete('auth_assignment', [
                            'user_id' => $user->id])->execute();
                            Yii::$app->db->createCommand()->insert('auth_assignment', [
                                'item_name' => $param['role'],
                                'user_id' => $user->id,
                            ])->execute();

                      }
                    
                }

                $this->id = $user->getId();
                return $user;
            }else{
                echo json_encode($user->getErrors());
            }
        }
        return null;
    }

    public function findUser($id){
        $model = User::findOne($id);
        $this->id = $model->id;
        $this->name =  $model->name;
        $this->surname = $model->surname;
        $this->email = $model->email;
        return $this;
    }

}
