<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use common\models\AuthAssignment;
use DomainException;
use InvalidArgumentException;
use UnexpectedValueException;
use DateTime;


// /**
//  * User model
//  *
//  * @property integer $id
//  * @property string $ username
//  * @property string $password_hash
//  * @property string $password_reset_token
//  * @property string $email
//  * @property string $auth_key
//  * @property integer $status
//
//  * @property integer $created_at
//  * @property integer $updated_at
//  *
//  * @property string $password write-only password
//  */


 /**
 * User model
 *
 * @property integer $id
 * @property string $name

 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $token
 * @property string $password write-only password
 */



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @SWG\Definition(required={"name", "email"})
 *
 * @SWG\Property(property="id", type="integer")
 * @SWG\Property(property="email", type="string")
 * @SWG\Property(property="name", type="string")
 */


class User extends ActiveRecord implements IdentityInterface
{
    use UserJwt;
    public $token;
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
          
        ];
    }
    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }
    /**
     * @inheritdoc
     */
    public function fields()
    {
        return ['name', 'surname', 'email', 'token', 'updated_at', 'created_at'];
    }
    /**
     * Finds user by username
     *
     * @param string $username
     * @return null or static
     */
    public static function findByUsername($username)
    {
        return static::find()
            ->select('id, name, email, password_hash, auth_key, created_at, updated_at')
            ->Where( ['name' => $username])
            ->one();
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
      // var_dump($token);
      // die();
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }
        return static::findOne([
            'password_reset_token' => $token,

        ]);
    }
    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
          if (empty($token)) {
            // echo "333";
            return false;
        }
        $timestamp = (int)substr($token, strrpos($token, '_') + 1);
        // var_dump($timestamp);
        $expire = Yii::$app->params['passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }
    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        parent::afterFind();
        $this->token = $this->getJWT();

        /* change format date */
        $parse = Yii::$app->formatter;
        $this->created_at = $parse->asDate($this->created_at, 'php:Y-m-d H:i:s');
        $this->updated_at = $parse->asDate($this->updated_at, 'php:Y-m-d H:i:s');
        // $f = $this->isPasswordResetTokenValid($this->token);
        // Yii::$app->response->headers->set('token', $this->token);
        Yii::$app->response->cookies->add(new \yii\web\Cookie(['name'=> 'token', 'value' => $this->token]));

    }
    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        $this->token = $this->getJWT();

        /* change format date */
        $parse = Yii::$app->formatter;
        $this->created_at = $parse->asDate($this->created_at, 'php:Y-m-d H:i:s');
        $this->updated_at = $parse->asDate($this->updated_at, 'php:Y-m-d H:i:s');
    }
    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }
    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
// var_dump($this->token);
      // var_dump($this->auth_key);
        return $this->auth_key;
    }
    public function getToken()
    {

      // var_dump($this->token);
        return $this->token;
    }
    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {

        return $this->getAuthKey() === $authKey;
    }
    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }
    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }
    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }
    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }
    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }


}
