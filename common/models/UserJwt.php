<?php
namespace common\models;
use Firebase\JWT\JWT;
use Yii;
use yii\web\Request as WebRequest;
/**
 * Trait to handle JWT-authorization process. Should be attached to User model.
 * If there are many applications using user model in different ways - best way
 * is to use this trait only in the JWT related part.
 */
trait UserJwt
{
    /**
     * Store JWT token header items.
     * @var array
     */
    protected static $decodedToken;
    /**
     * Getter for secret key that's used for generation of JWT
     * @return string secret key used to generate JWT
     */
    protected static function getSecretKey()
    {
      return isset(Yii::$app->params['secretKey']) ? Yii::$app->params['secretKey'] : 'tokenDefault';
    }
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }
    /**
     * Getter for expIn token that's used for generation of JWT
     * @return integer time to add expIn token used to generate JWT
     */
    protected static function getExpireIn()
    {
      // var_dump(Yii::$app->params['expiresIn']);
      // die();
      return isset(Yii::$app->params['expiresIn']) ? strtotime(Yii::$app->params['expiresIn']) : 0;
    }
    /**
     * Getter for "header" array that's used for generation of JWT
     * @return array JWT Header Token param, see http://jwt.io/ for details
     */
    protected static function getHeaderToken()
    {
        return [];
    }
    /**
     * Logins user by given JWT encoded string. If string is correctly decoded
     * - array (token) must contain 'jti' param - the id of existing user
     * @param  string $accessToken access token to decode
     * @return mixed|null          User model or null if there's no user
     * @throws \yii\web\ForbiddenHttpException if anything went wrong
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        try {
            $decoded = JWT::decode($token, $secret, [static::getAlgo()]);
        } catch (\Exception $e) {
            return false;
        }
        static::$decodedToken = (array)$decoded;
        // If there's no jti param - exception
        if (!isset(static::$decodedToken['jti'])) {
            return false;
        }
        // JTI is unique identifier of user.
        // For more details: https://tools.ietf.org/html/rfc7519#section-4.1.7
        $id = static::$decodedToken['jti'];
        return static::findByJTI($id);
    }
    /**
     * Finds User model using static method findOne
     * Override this method in model if you need to complicate id-management
     * @param  string $id if of user to search
     * @return mixed       User model
     */
    public static function findByJTI($token)
    {
        return static::findOne(['auth_key' => $token]);
    }
    /**
     * Getter for encryption algorytm used in JWT generation and decoding
     * Override this method to set up other algorytm.
     * @return string needed algorytm
     */
    public static function getAlgo()
    {
        return 'HS256';
    }
    /**
     * Returns some 'id' to encode to token. By default is current model id.
     * If you override this method, be sure that findByJTI is updated too
     * @return integer any unique integer identifier of user
     */
    public function getJTI()
    {
        return $this->getAuthKey();
    }
    public static function findRole()
    {

    }
    /**
     * Encodes model data to create custom JWT with model.id set in it
     * @return string encoded JWT
     */
    public function getJWT()
    {
        // Collect all the data
        // var_dump(Yii::$app->response->headers->get('token'));
        // die();
        // if(empty(Yii::$app->response->headers->get('token'))){
        $role = array();
        $rol = 'guest';
        $id = User::getId();

        $role =Yii::$app->db->createCommand('SELECT item_name FROM auth_assignment WHERE user_id ='. $id)->queryAll(\PDO::FETCH_COLUMN);
// var_dump($role);
// die();
        foreach ($role as $key => $value) {
          $rol = $role[0];
        }
        $secret = static::getSecretKey();
        // var_dump($secret);
        // die();
        $currentTime = time();
        $request = Yii::$app->request;
        // var_dump($request);
        $hostInfo = '';
        // There is also a \yii\console\Request that doesn't have this property
        if ($request instanceof WebRequest) {
            $hostInfo = $request->hostInfo;
        }
        // Merge token with presets not to miss any params in custom
        // configuration
        $token = array_merge([
            'iss' => $hostInfo,
            'aud' => $hostInfo,
            'iat' => $currentTime,
            'nbf' => $currentTime,
            'exp' => static::getExpireIn(),
            'role' => $rol

        ], static::getHeaderToken());
        // Set up id
        $token['jti'] = $this->getJTI();
        return JWT::encode($token, $secret, static::getAlgo());
      // }else{
      //   $jwt = JWT::decode(Yii::$app->response->headers->get('token'), Yii::$app->params['secretKey'],array(Yii::$app->params['algo']));
      //   if(!$jwt){
      //     return Yii::$app->response->redirect(['user/login']);
      //   }
      //
      // }

    }
}
