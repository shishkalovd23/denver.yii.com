<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'secretKey' => '1234567890',
    'expiresIn' => '+3 min',
    'algo' => 'HS256'

];
