
## Конфигурация
### Apache

Для нормального функционирования проекта необходимо развернуть на сервере  
web-server Apache2+ , PHP 7.1 +  MySQL Ver 15.1 Distrib 10.2.6-MariaDB и выше 
  
Активировать ReWrite mod для apache.  
  
  $ sudo a2enmod rewrite  
  
В конфигурационном файле Apache для корневой директории необходимо написать следующее:  
  

`<Directory /var/www/>`  
   
        Options Indexes FollowSymLinks  
         AllowOverride All  
        Require all granted  
`</Directory>`    


### PHP  
Проект работает на версии 7.1.  
Для нормальной работы проекта в файле /etc/php/7.1/apache2/php.ini  
необходимо сделать следующие настройки:


        short_open_tag = On  
        memory_limit = -1  
        
        
### Вход  
Для входа имя пользователя: admin, пароль: 123456  
  
Из корня проекта импортировать файл developer.sql в базу developer предварительно
созданную на SQL сервере.



