CREATE DATABASE  IF NOT EXISTS `developer` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `developer`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: developer
-- ------------------------------------------------------
-- Server version	5.5.5-10.2.7-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_assignment`
--

DROP TABLE IF EXISTS `auth_assignment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Название роли',
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL COMMENT 'дата создания',
  PRIMARY KEY (`item_name`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_assignment`
--

LOCK TABLES `auth_assignment` WRITE;
/*!40000 ALTER TABLE `auth_assignment` DISABLE KEYS */;
INSERT INTO `auth_assignment` VALUES ('editor','265',1522507306),('editor','270',NULL),('user_admin','126',1473330578),('user_admin','127',1473682829),('user_admin','129',1475156071),('user_admin','13',1456222582),('user_admin','138',1475676261),('user_admin','141',1482220383),('user_admin','142',1484308086),('user_admin','143',1484309614),('user_admin','146',1492604909),('user_admin','150',1495447543),('user_admin','152',1495459609),('user_admin','157',1502363481),('user_admin','158',1502363447),('user_admin','159',1502363401),('user_admin','160',1502363271),('user_admin','161',1502362949),('user_admin','162',1502362937),('user_admin','165',1502090241),('user_admin','169',1502091128),('user_admin','170',1502091344),('user_admin','19',1458569945),('user_admin','22',1458736818),('user_admin','227',1504185517),('user_admin','266',NULL),('user_admin','267',NULL),('user_admin','269',NULL),('user_admin','276',NULL),('user_admin','277',NULL),('user_admin','32',1463394616),('user_admin','34',1502364855),('user_admin','35',1464349791),('user_admin','38',1467384393),('user_admin','43',1469451420),('user_admin','72',1463043576),('user_admin','74',1522521969),('user_admin','75',1463043576),('user_admin','86',NULL);
/*!40000 ALTER TABLE `auth_assignment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_item`
--

DROP TABLE IF EXISTS `auth_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_item` (
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL COMMENT 'тип: роль или дейстаие',
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'описание роли',
  `rule_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'название бизнес правила',
  `data` text CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL COMMENT 'дата создания',
  `updated_at` int(11) DEFAULT NULL COMMENT 'таба обновления',
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `idx-auth_item-type` (`type`),
  CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_item`
--

LOCK TABLES `auth_item` WRITE;
/*!40000 ALTER TABLE `auth_item` DISABLE KEYS */;
INSERT INTO `auth_item` VALUES ('editor',1,NULL,NULL,NULL,NULL,NULL),('user_admin',1,NULL,NULL,NULL,1456221456,1456221456);
/*!40000 ALTER TABLE `auth_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_item_child`
--

DROP TABLE IF EXISTS `auth_item_child`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item_old` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item_old` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_item_child`
--

LOCK TABLES `auth_item_child` WRITE;
/*!40000 ALTER TABLE `auth_item_child` DISABLE KEYS */;
INSERT INTO `auth_item_child` VALUES ('accountant','administration'),('asm','users'),('developer','administration'),('director','administration'),('driver','users'),('support','administration'),('user_admin','users');
/*!40000 ALTER TABLE `auth_item_child` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_item_group`
--

DROP TABLE IF EXISTS `auth_item_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_item_group` (
  `id` int(11) NOT NULL,
  `child_group` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `parent_group` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_item_group`
--

LOCK TABLES `auth_item_group` WRITE;
/*!40000 ALTER TABLE `auth_item_group` DISABLE KEYS */;
INSERT INTO `auth_item_group` VALUES (1,'routes_create','routes'),(2,'routes_delete','routes'),(3,'routes_edit','routes'),(4,'routes_show_all','routes'),(5,'routes_show_personal','routes'),(6,'personal_point','geozone'),(7,'main_geozone_show_personal','geozone'),(8,'main_geozone_show_all','geozone'),(9,'main_geozone_edit','geozone'),(10,'main_geozone_delete','geozone'),(11,'main_geozone_create','geozone'),(12,'main_geozone','geozone'),(13,'geozone_group_show_personal','geozone'),(14,'geozone_group_show_all','geozone');
/*!40000 ALTER TABLE `auth_item_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_rule`
--

DROP TABLE IF EXISTS `auth_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT 'имя бизнес правила',
  `data` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_rule`
--

LOCK TABLES `auth_rule` WRITE;
/*!40000 ALTER TABLE `auth_rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(45) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'Спортeee',1522494072,1523250853),(2,'Политика',1522494072,NULL),(3,'Наука',1522494072,1523339677),(4,'Погода',1522494072,NULL);
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration`
--

DROP TABLE IF EXISTS `migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration`
--

LOCK TABLES `migration` WRITE;
/*!40000 ALTER TABLE `migration` DISABLE KEYS */;
INSERT INTO `migration` VALUES ('m000000_000000_base',1522398432),('m130524_201442_init',1522398437);
/*!40000 ALTER TABLE `migration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` longtext NOT NULL,
  `content` longtext DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `categories` varchar(45) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news`
--

LOCK TABLES `news` WRITE;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
INSERT INTO `news` VALUES (1,'Новость 1','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.consequat.sfdfsdfsdfsf</p>',0,'Спорт',1522494072,1523250824),(2,'Новость 2','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.consequat.</p>',1,NULL,1522494072,1522835651),(3,'Новость 3','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.consequat.',1,NULL,1522494072,NULL),(5,'sadasdasdasdasdasdasdsdfsdfsdf222','<p>asfadfasfasfdasfasfafsasfafsfsfdsfdsfsdfsdfsdf</p>',1,'Спортeee',1522577573,1523339714),(13,'sdscvxzfzxfzdxczczczxczczxczxc','<p>zxczxcaszdzdfcvzdfczfczcxzcx</p>',1,NULL,1523019357,1523019357),(15,'scxzzxczxczxczxc','<p>czxczx232134124423czxczxczvxcvc</p>',1,NULL,1523019645,1523019645);
/*!40000 ALTER TABLE `news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news_assignment`
--

DROP TABLE IF EXISTS `news_assignment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news_assignment` (
  `id_news` int(11) NOT NULL,
  `id_categories` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news_assignment`
--

LOCK TABLES `news_assignment` WRITE;
/*!40000 ALTER TABLE `news_assignment` DISABLE KEYS */;
INSERT INTO `news_assignment` VALUES (2,4),(1,2),(5,2),(15,4);
/*!40000 ALTER TABLE `news_assignment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `socket_id` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `surname` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`)
) ENGINE=InnoDB AUTO_INCREMENT=278 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (74,'Денис','','Шишка','POQ-eyb2XxvKl1odufzOgnKJ-GoWxsgA','$2y$13$3q2lFQku1YgKafwd3HRV1u/JT1.wpVsTNifQJ.pHLG7WZuKz9PTp2',NULL,'help@gpsua.com',1471855640,1522661756),(265,'denis',NULL,'denver','q_FcuM8OSpfaz9DzVez9g3aVphfrKCBN','$2y$13$GTEetgzc33zyXlAbI5ktPuJwf5tMx3bIZVVyR05JfX4v7vYZShQz.',NULL,'denis@denis.com',1522498879,1522498879),(266,'Денис',NULL,'Шишкалов','xmNZE9aSQkUyGf-D8WaQZctnZ9RsJYXk','$2y$13$MRwGWBEIU1V1rg9C2RAZMOahlu7oEjhPIQhW5ghBShlPghzerHBa2',NULL,'denver@gmail.com',1522523352,1522523417),(267,'sdfsafds',NULL,'sdfsdf','KS1X6AiEGsGmDDH4rOW1LHUggmkuz3ew','$2y$13$VNq/5.Iy4/9HsWwf7nH2WelDYKZ0Yg3Q9ZPB1v3CQ9dzBPKmscoDi',NULL,'sdfsdfsd@asdfsdfs.com',1522560330,1522560330),(269,'admin',NULL,'admin','Y0uPLksD1WEJHma9P8DCovNOCqiJ9Wke','$2y$13$b4tFSyCwjvON464Pbpos9.N03sTRDHDHLOiuIhVj/rUkjsy12jOCS',NULL,'admin@admin.com',2018,1523342153),(270,'editor',NULL,'editor','CJWOB14Y2Ky3YmYzDo7bPy6zkYOM5jai','$2y$13$w.YP9P2TgBo45bXJD/uHk.aqYRxPmw/Q8i8oeu4gDTXmnEm1/PkNO',NULL,'editor@editor.com',1522745563,1522745563),(271,'test',NULL,'test','RX-fJO4c-ai7H3BkU53acp3D3JY2PeSr','$2y$13$LncigLx/ftfnxgtgP3TShuexA1D53W09mdYukpqa4JjURH.w6V0Ay',NULL,'test@test.com',1970,1523038531),(276,'asdsdfas',NULL,'asfdsdfsdf','5SrdhMFe4WpJHGMr27y8KY7CwC9wmaLL','$2y$13$3XWLqGeGH8Sbj1bCafwP7OaRXWmVFJhIp7Ril0GR0Gz0YI4PUnaWG',NULL,'asdfsdf@dfsdf.com',1523439524,1523439524),(277,'sszdsdsz',NULL,'zxczxczxczx','D2q4Rwwq2DzMrXCIRI77WMNvxccbewl9','$2y$13$5F/qNEc7WoDBWqyzoQD4XOQzC64AC0Ri4eiZdDVgyiKrgvE8GoE2u',NULL,'sczxczxc@sdfsdfsd.sdfsf',1523443971,1523443971);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_assigned_groups`
--

DROP TABLE IF EXISTS `users_assigned_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_assigned_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL DEFAULT 0,
  `transport_group` int(11) NOT NULL DEFAULT 0,
  `geozones_group` int(11) NOT NULL DEFAULT 0,
  `access_group` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=329 DEFAULT CHARSET=cp1251;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_assigned_groups`
--

LOCK TABLES `users_assigned_groups` WRITE;
/*!40000 ALTER TABLE `users_assigned_groups` DISABLE KEYS */;
INSERT INTO `users_assigned_groups` VALUES (1,45,10,0,0),(2,46,1,0,0),(3,47,1,0,0),(4,48,1,0,0),(5,49,1,0,0),(7,51,2,0,0),(9,52,1,0,0),(10,53,1,0,0),(11,53,2,0,0),(12,53,3,0,0),(13,55,1,0,0),(14,55,2,0,0),(15,55,3,0,0),(16,57,1,0,0),(17,58,1,0,0),(18,58,2,0,0),(19,59,2,0,0),(22,62,2,0,0),(23,63,1,0,0),(24,63,2,0,0),(25,60,2,0,0),(26,60,3,0,0),(89,65,2,0,0),(90,66,1,0,0),(91,67,1,0,0),(92,68,1,0,0),(93,69,1,0,0),(94,70,2,0,0),(95,71,1,0,0),(116,79,14,0,0),(117,79,13,0,0),(118,79,12,0,0),(119,79,0,15,0),(126,78,14,0,0),(127,78,13,0,0),(128,78,12,0,0),(129,78,0,15,0),(135,77,14,0,0),(136,77,13,0,0),(137,77,12,0,0),(165,80,0,15,0),(198,81,14,0,0),(199,81,12,0,0),(200,82,14,0,0),(201,82,13,0,0),(202,82,12,0,0),(203,83,14,0,0),(204,83,13,0,0),(205,84,14,0,0),(206,84,12,0,0),(207,85,12,0,0),(225,50,1,0,0),(226,50,0,15,0),(229,124,16,0,0),(230,124,20,0,0),(237,125,25,0,0),(238,125,16,0,0),(239,125,27,0,0),(240,125,20,0,0),(241,125,22,0,0),(242,125,29,0,0),(243,125,23,0,0),(244,125,30,0,0),(245,125,33,0,0),(246,125,26,0,0),(247,125,28,0,0),(249,145,1,0,0),(271,163,0,306,0),(274,202,38,0,0),(275,202,0,286,0),(276,203,36,0,0),(277,203,0,306,0),(291,207,0,286,0),(292,156,0,301,0),(299,154,0,288,0),(302,181,0,300,0),(303,181,0,282,0),(304,181,0,302,0),(305,181,0,298,0),(311,211,0,288,0),(312,211,0,300,0),(313,211,0,301,0),(314,211,0,298,0),(317,205,36,0,0),(318,205,0,306,0),(319,204,38,0,0),(322,204,37,0,0),(323,230,0,11,0),(324,230,0,2,0),(326,231,0,2,0),(327,231,0,15,0),(328,31,38,0,0);
/*!40000 ALTER TABLE `users_assigned_groups` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-04-11 13:55:57
